import streamlit as st
from annotated_text import annotated_text

from haystack.document_stores import ElasticsearchDocumentStore
from haystack.utils import clean_wiki_text, convert_files_to_dicts
from haystack.nodes import FARMReader, ElasticsearchRetriever
from haystack.pipelines import ExtractiveQAPipeline

document_store = ElasticsearchDocumentStore(
    host="elasticsearch", username="", password="", index="document")
retriever = ElasticsearchRetriever(document_store=document_store)
reader = FARMReader(model_name_or_path="z-uo/bert-qasper", use_gpu=False)
pipe = ExtractiveQAPipeline(reader, retriever)
doc_dir = 'src'

selectbox_options = ("Query", "Add document")
selectbox = st.sidebar.selectbox(
    "Select Options",
    selectbox_options
)
if selectbox == selectbox_options[0]:
    st.title('Query')
    question=st.text_input("Do a query", "What they propose?")

    if question:
        prediction = pipe.run(
            query=question, 
            params={
                "Retriever": {
                    "top_k": 4
                }, 
                "Reader": {
                    "top_k": 2
                }
            }
        )

        if len(prediction['answers']) == 0:
            'no answers found'
        else:
            for answer in prediction['answers']:
                start = answer.offsets_in_context[0].start
                end = answer.offsets_in_context[0].end
                context = answer.context
                annotated_text(
                    context[0:start],
                    (answer.answer,"","#2596be"),
                    context[end:],
                )
                '**Relevance:** ', answer.score , '**source:** ' , answer.meta['name']
else:
    st.title('Add document')
    st.text(f"I add all .txt under {doc_dir}.")
    dicts = convert_files_to_dicts(
        dir_path=doc_dir, clean_func=clean_wiki_text, split_paragraphs=True)
    document_store.write_documents(dicts)
