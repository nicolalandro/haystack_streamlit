FROM python:3.8

# for streamlit
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
RUN mkdir -p /root/.streamlit
RUN bash -c 'echo -e "\
[general]\n\
email = \"\"\n\
" > /root/.streamlit/credentials.toml'
RUN bash -c 'echo -e "\
[server]\n\
enableCORS = false\n\
" > /root/.streamlit/config.toml'

WORKDIR /code
ADD requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt
RUN python -c "from transformers import pipeline; model_name = 'z-uo/bert-qasper'; pipeline('question-answering', model=model_name, tokenizer=model_name)"
COPY src ./src

CMD streamlit run src/app.py --server.port 8501
