# Haystack tutorial with Streamlit UI
This project contains a streamlit demo connected to elastic search that use haystack for question answering.

![question image](imgs/Query.png)

## Features

* add documents
* query

## How to run

* docker-compose up
* at first go to add documet
* at second ask a question

## Bibliography
* [tutorial Haystack](https://github.com/deepset-ai/haystack/blob/master/tutorials/Tutorial1_Basic_QA_Pipeline.ipynb)
* [Streamlit Haystack demo](https://gist.github.com/demarant/d9e617fb80057692f2f7cddf00013b9b)

